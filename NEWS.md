Major changes in Nikita Noark 5 Core releases
=============================================

These are the highlevel changes.  For details, see the git history.

Release 0.3 UNRELEASED
----------------------
 * Improved ClassificationSystem and Class behaviour.
 * Tidied up known inconsistencies between domain model and hateaos links.
 * Added experimental code for blockchain integration. 	
 * Make token expiry time configurable at upstart from properties file.
 * Continued work on OData search syntax.
 * Started work on pagination for entities, partly implemented for Saksmappe.
 * Finalise ClassifiedCode Metadata entity.
 * Implement mechanism to check if authentication token is still
   valid.  This allow the GUI to return a more sensible message to the
   user if the token is expired.
 * Reintroduce browse.html page to allow user to browse JSON API using
   hateoas links.
 * Fix bug in handling file/mappe sequence number.  Year change was
   not properly handled.
 * Update application yml files to be in sync with current development.
 * Stop 'converting' everything to PDF using libreoffice.  Only
   convert the file formats doc, ppt, xls, docx, pptx, xlsx, odt, odp
   and ods.
 * Continued code style fixing, making code more readable.
 * Minor bug fixes.

Release 0.2.1 2018-11-02 (commit 2bf7dfb7f39067c09b9db1d473b4b2c42cd602de)
----------------------
 * Fixed bug resulting in a class cast exception
 * Corrected handling of administrativEnhet.
 * Fixed bug in some \_links href entries containing double slashes.
 * Reintroduce Allow headers for OPTIONS.
 * Fixed bug when creating saksmappe, failing to find the correct
   administrativEnhet when there were several with identical names.
 * Fixed bug causing empty \_links when creating dokumentbeskrivelse.

Release 0.2 2018-10-17 (commit d7c3feaa945e2b0cfc19055a201091c7b3840b89)
------------------------
 * Fix typos in REL names
 * Tidy up error message reporting
 * Fix issue where we used Integer.valueOf(), not Integer.getInteger()
 * Change some String handling to StringBuffer
 * Fix error reporting
 * Code tidy-up
 * Fix issue using static non-synchronized SimpleDateFormat to avoid 
   race conditions
 * Fix problem where deserialisers were treating integers as strings
 * Update methods to make them null-safe
 * Fix many issues reported by coverity
 * Improve equals(), compareTo() and hash() in domain model
 * Improvements to the domain model for metadata classes
 * Fix CORS issues when downloading document
 * Implementation of case-handling with registryEntry and document upload
 * Better support in Javascript for OPTIONS
 * Adding concept description of mail integration
 * Improve setting of default values for GET on ny-journalpost
 * Better handling of required values during deserialisation 
 * Changed tilknyttetDato (M620) from date to dateTime
 * Corrected some opprettetDato (M600) (de)serialisation errors.
 * Improve parse error reporting.
 * Started on OData search and filtering.
 * Added Contributor Covenant Code of Conduct to project.
 * Moved repository and project from Github to Gitlab.
 * Restructured repository, moved code into src/ and web/.
 * Updated code to use Spring Boot version 2.
 * Added support for OAuth2 authentication.
 * Fixed several bugs discovered by Coverity.
 * Corrected handling of date/datetime fields.
 * Improved error reporting when rejecting during deserializatoin.
 * Adjusted default values provided for ny-arkivdel, ny-mappe,
   ny-saksmappe, ny-journalpost and ny-dokumentbeskrivelse.
 * Several fixes for korrespondansepart*.
 * Updated web GUI:
    - Now handle both file upload and download.
    - Uses new OAuth2 authentication for login.
    - Forms now fetches default values from API using GET.
    - Added RFC 822 (email), TIFF and JPEG to list of possible file formats.

Release 0.1.1 2017-06-09 (commit a3932c87b22aee272e2a0385bb8a7d029a73faf4)
--------------------------------------------------------------------------
 * Continued work on the angularjs GUI, including document upload
 * Implemented correspondencepartPerson, correspondencepartUnit and correspondencepartInternal
 * Applied for coverity coverage and started submitting code on regular basis
 * Started fixing bugs reported by coverity
 * Corrected and completed HATEOAS links to make sure entire API is
   available via URLs in \_links.
 * Corrected all relation URLs to use trailing slash.
 * Add initial support for storing data in ElasticSearch.
 * Now able to receive and store uploaded files in the archive.
 * Changed JSON output for object lists to have relations in \_links.
 * Improve JSON output for empty object lists.
 * Now uses correct MIME type application/vnd.noark5-v4+json.
 * Added support for docker container images
 * Added simple API browser implemented in JavaScript/Angular.
 * Started on archive client implemented in JavaScript/Angular.
 * Started on prototype to show the public mail journal.
 * Improved performance by disabling Sprint FileWatcher.
 * Added support for 'arkivskaper', 'saksmappe' and 'journalpost'.
 * Added support for some metadata codelists.
 * Added support for Cross-origin resource sharing (CORS).
 * Changed login method from Basic Auth to JSON Web Token (RFC 7519) style.
 * Added support for GET-ing ny-* URLs.
 * Added support for modifying entities using PUT and eTag.
 * Added support for returning XML output on request.
 * Removed support for English field and class names, limiting ourself
   to the official names.
 * ...

Release 0.1 2017-01-31 (commit 6ec4acb9c1d5b72fd4bf58074769233e78483bb4)
-----------------------
 * Able to store archive metadata.
