#!/usr/bin/env bash

if [ $# -eq 0 ]
  then
    echo "No arguments supplied"
    exit;
fi

CURL_RESPONSE=`curl -X POST  --data-urlencode "directory=$1" 'http://localhost:9191/metadata/'`
echo $CURL_RESPONSE;
