package no.oslomet.metadata.controller;

import no.oslomet.metadata.service.IFileService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.io.File;

import static no.oslomet.metadata.utils.Constants.SLASH;

/**
 * Created by tsodring on 24/01/19.
 * <p>
 * RestController for the Metadata application
 * <p>
 * Simply allows you to call processing on a given directory
 */
@RestController
@RequestMapping(value = SLASH)
public class AppController {

    private IFileService fileService;

    public AppController(IFileService fileService) {
        this.fileService = fileService;
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<String> processDirectory(String directory) {
        fileService.processDirectory(new File(directory));
        String result = "Directory [ "+ directory +"] processed successfully";
        return ResponseEntity.status(HttpStatus.CREATED).
                body(result);
    }
}
