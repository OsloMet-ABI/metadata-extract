package no.oslomet.metadata.model;

import javax.persistence.*;

@Entity
@Table(name = "metadata")
public class FileMetadata {

    @Id
    @GeneratedValue
    @Column(name = "id")
    private Long id;

    @Column(name = "file_name")
    private String fileName;

    @Column(name = "directory")
    private String directory;

    @Column(name = "file_size")
    private Long fileSize;

    @Column(name = "file_last_printed_date_time")
    private String fileLastPrintedDateTime;

    @Column(name = "file_last_printed_date")
    private String fileLastPrintedDate;

    @Column(name = "file_last_printed_time")
    private String fileLastPrintedTime;

    @Column(name = "file_creation_date_time")
    private String fileCreationDateTime;

    @Column(name = "file_creation_date")
    private String fileCreationDate;

    @Column(name = "file_creation_time")
    private String fileCreationTime;

    @Column(name = "file_modification_date_time")
    private String fileModificationDateTime;

    @Column(name = "file_modification_date")
    private String fileModificationDate;

    @Column(name = "file_modification_time")
    private String fileModificationTime;

    @Column(name = "file_access_date_time")
    private String fileAccessDateTime;

    @Column(name = "file_access_date")
    private String fileAccessDate;

    @Column(name = "file_access_time")
    private String fileAccessTime;

    @Column(name = "file_save_date_time")
    private String fileSaveDateTime;

    @Column(name = "file_save_date")
    private String fileSaveDate;

    @Column(name = "file_save_time")
    private String fileSaveTime;

    @Column(name = "mime_type")
    private String mimeType;

    @Column(name = "total_edit_time")
    private String totalEditTime;

    @Column(name = "title")
    private String title;

    @Column(name = "description")
    private String description;

    @Column(name = "author")
    private String author;

    @Column(name = "keywords")
    private String keywords;

    @Column(name = "company")
    private String company;

    @Column(name = "creator")
    private String creator;

    @Column(name = "modifier")
    private String modifier;

    @Column(name = "subject")
    private String subject;

    @Column(name = "last_edited_by")
    private String lastEditedBy;

    @Column(name = "language")
    private String language;

    @Column(name = "comments")
    private String comments;

    @Column(name = "creator_tool")
    private String creatorTool;

    @Column(name = "format")
    private String format;

    @Column(name = "identifier")
    private String identifier;

    @Column(name = "metadata_date")
    private String metadataDate;

    @Column(name = "character_count")
    private Long characterCount;

    @Column(name = "word_count")
    private Long wordCount;

    @Column(name = "page_count")
    private Long pageCount;


    public FileMetadata(String fileName, String directory, Long fileSize,
                        String fileLastPrintedDateTime,
                        String fileCreationDateTime, String fileAccessDateTime,
                        String fileSaveDateTime, String mimeType,
                        String totalEditTime, String title,
                        String author, String keywords, String company,
                        String creator, String modifier, String subject,
                        String lastEditedBy, Long characterCount,
                        Long wordCount, Long pageCount,
                        String language, String description, String comments,
                        String creatorTool, String format,
                        String identifier, String metadataDate) {
        this.fileName = fileName;
        this.directory = directory;
        this.fileSize = fileSize;

        this.fileLastPrintedDateTime = fileLastPrintedDateTime;
        if (fileLastPrintedDateTime != null) {
            String[] fileLastPrinted = fileLastPrintedDateTime.split("\\s*T\\s*");
            if (fileLastPrinted .length == 2) {
                this.fileLastPrintedDate = fileLastPrinted[0];
                this.fileLastPrintedTime = fileLastPrinted[1];
            }
        }

        this.fileCreationDateTime = fileCreationDateTime;
        if (fileLastPrintedDateTime != null) {
            String[] fileCreation= fileCreationDateTime.split("\\s*T\\s*");
            if (fileCreation.length == 2) {
                this.fileCreationDate = fileCreation[0];
                this.fileCreationTime = fileCreation[1];
            }
        }

        this.fileAccessDateTime = fileAccessDateTime;
        if (fileAccessDateTime != null) {
            String[] fileAccess = fileAccessDateTime.split("\\s*T\\s*");
            if (fileAccess.length == 2) {
                this.fileAccessDate= fileAccess[0];
                this.fileAccessTime = fileAccess[1];
            }
        }

        this.fileSaveDateTime = fileSaveDateTime;
        if (fileSaveDateTime != null) {
            String[] fileSave = fileSaveDateTime.split("\\s*T\\s*");
            if (fileSave.length == 2) {
                this.fileSaveDate = fileSave[0];
                this.fileSaveTime= fileSave[1];
            }
        }

        this.mimeType = mimeType;
        this.totalEditTime = totalEditTime;
        this.title = title;
        this.author = author;
        this.keywords = keywords;
        this.company = company;
        this.creator = creator;
        this.modifier = modifier;
        this.subject = subject;
        this.lastEditedBy = lastEditedBy;
        this.characterCount = characterCount;
        this.wordCount = wordCount;
        this.pageCount = pageCount;
        this.language = language;
        this.description = description;
        this.comments = comments;
        this.creatorTool = creatorTool;
        this.format = format;
        this.identifier = identifier;
        this.metadataDate = metadataDate;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getDirectory() {
        return directory;
    }

    public void setDirectory(String directory) {
        this.directory = directory;
    }

    public Long getFileSize() {
        return fileSize;
    }

    public void setFileSize(Long fileSize) {
        this.fileSize = fileSize;
    }

    public String getFileLastPrintedDateTime() {
        return fileLastPrintedDateTime;
    }

    public void setFileLastPrintedDateTime(String fileLastPrintedDateTime) {
        this.fileLastPrintedDateTime = fileLastPrintedDateTime;
    }

    public String getFileLastPrintedDate() {
        return fileLastPrintedDate;
    }

    public void setFileLastPrintedDate(String fileLastPrintedDate) {
        this.fileLastPrintedDate = fileLastPrintedDate;
    }

    public String getFileLastPrintedTime() {
        return fileLastPrintedTime;
    }

    public void setFileLastPrintedTime(String fileLastPrintedTime) {
        this.fileLastPrintedTime = fileLastPrintedTime;
    }

    public String getFileCreationDateTime() {
        return fileCreationDateTime;
    }

    public void setFileCreationDateTime(String fileCreationDateTime) {
        this.fileCreationDateTime = fileCreationDateTime;
    }

    public void setFileModificationDateTime(String fileModificationDateTime) {
        this.fileModificationDateTime = fileModificationDateTime;
    }

    public String getFileCreationDate() {
        return fileCreationDate;
    }

    public void setFileCreationDate(String fileCreationDate) {
        this.fileCreationDate = fileCreationDate;
    }

    public String getFileCreationTime() {
        return fileCreationTime;
    }

    public void setFileCreationTime(String fileCreationTime) {
        this.fileCreationTime = fileCreationTime;
    }

    public String getFileAccessDateTime() {
        return fileAccessDateTime;
    }

    public void setFileAccessDateTime(String fileAccessDateTime) {
        this.fileAccessDateTime = fileAccessDateTime;
    }

    public String getFileAccessDate() {
        return fileAccessDate;
    }

    public void setFileAccessDate(String fileAccessDate) {
        this.fileAccessDate = fileAccessDate;
    }

    public String getFileAccessTime() {
        return fileAccessTime;
    }

    public void setFileAccessTime(String fileAccessTime) {
        this.fileAccessTime = fileAccessTime;
    }

    public String getFileSaveDateTime() {
        return fileSaveDateTime;
    }

    public void setFileSaveDateTime(String fileSaveDateTime) {
        this.fileSaveDateTime = fileSaveDateTime;
    }

    public String getFileSaveDate() {
        return fileSaveDate;
    }

    public void setFileSaveDate(String fileSaveDate) {
        this.fileSaveDate = fileSaveDate;
    }

    public String getFileSaveTime() {
        return fileSaveTime;
    }

    public void setFileSaveTime(String fileSaveTime) {
        this.fileSaveTime = fileSaveTime;
    }

    public String getMimeType() {
        return mimeType;
    }

    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }

    public String getTotalEditTime() {
        return totalEditTime;
    }

    public void setTotalEditTime(String totalEditTime) {
        this.totalEditTime = totalEditTime;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getKeywords() {
        return keywords;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public String getModifier() {
        return modifier;
    }

    public void setModifier(String modifier) {
        this.modifier = modifier;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getLastEditedBy() {
        return lastEditedBy;
    }

    public void setLastEditedBy(String lastEditedBy) {
        this.lastEditedBy = lastEditedBy;
    }

    public Long getCharacterCount() {
        return characterCount;
    }

    public void setCharacterCount(Long characterCount) {
        this.characterCount = characterCount;
    }

    public Long getWordCount() {
        return wordCount;
    }

    public void setWordCount(Long wordCount) {
        this.wordCount = wordCount;
    }

    public Long getPageCount() {
        return pageCount;
    }

    public void setPageCount(Long pageCount) {
        this.pageCount = pageCount;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCreatorTool() {
        return creatorTool;
    }

    public void setCreatorTool(String creatorTool) {
        this.creatorTool = creatorTool;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    @Override
    public String toString() {
        return "FileMetadata{" +
                "id=" + id +
                ", fileName='" + fileName + '\'' +
                ", directory='" + directory + '\'' +
                ", fileSize=" + fileSize +
                ", fileLastPrintedDateTime='" + fileLastPrintedDateTime + '\'' +
                ", fileLastPrintedDate='" + fileLastPrintedDate + '\'' +
                ", fileLastPrintedTime='" + fileLastPrintedTime + '\'' +
                ", fileCreationDateTime='" + fileCreationDateTime + '\'' +
                ", fileCreationDate='" + fileCreationDate + '\'' +
                ", fileCreationTime='" + fileCreationTime + '\'' +
                ", fileModificationDateTime='" + fileModificationDateTime + '\'' +
                ", fileModificationDate='" + fileModificationDate + '\'' +
                ", fileModificationTime='" + fileModificationTime + '\'' +
                ", fileAccessDateTime='" + fileAccessDateTime + '\'' +
                ", fileAccessDate='" + fileAccessDate + '\'' +
                ", fileAccessTime='" + fileAccessTime + '\'' +
                ", fileSaveDateTime='" + fileSaveDateTime + '\'' +
                ", fileSaveDate='" + fileSaveDate + '\'' +
                ", fileSaveTime='" + fileSaveTime + '\'' +
                ", mimeType='" + mimeType + '\'' +
                ", totalEditTime='" + totalEditTime + '\'' +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", author='" + author + '\'' +
                ", keywords='" + keywords + '\'' +
                ", company='" + company + '\'' +
                ", creator='" + creator + '\'' +
                ", modifier='" + modifier + '\'' +
                ", subject='" + subject + '\'' +
                ", lastEditedBy='" + lastEditedBy + '\'' +
                ", language='" + language + '\'' +
                ", comments='" + comments + '\'' +
                ", creatorTool='" + creatorTool + '\'' +
                ", format='" + format + '\'' +
                ", characterCount=" + characterCount +
                ", wordCount=" + wordCount +
                ", pageCount=" + pageCount +
                '}';
    }
}
