package no.oslomet.metadata.model;

public class FileMetadataBuilder {
    private String fileName;
    private String directory;
    private Long fileSize;
    private String fileLastPrintedDateTime;
    private String fileCreationDateTime;
    private String fileModificationDateTime ;
    private String fileAccessDateTime;
    private String fileSaveDateTime;
    private String mimeType;
    private String totalEditTime;
    private String title;
    private String author;
    private String keywords;
    private String company;
    private String creator;
    private String modifier;
    private String subject;
    private String lastEditedBy;
    private Long wordCount;
    private Long pageCount;
    private Long characterCount;
    private String language;
    private String description;
    private String comments;
    private String creatorTool;
    private String format;
    private String identifier;
    private String metadataDate;

    public FileMetadataBuilder setFileName(String fileName) {
        this.fileName = fileName;
        return this;
    }

    public FileMetadataBuilder setDirectory(String directory) {
        this.directory = directory;
        return this;
    }

    public FileMetadataBuilder setFileSize(Long fileSize) {
        this.fileSize = fileSize;
        return this;
    }

    public FileMetadataBuilder setFileLastPrintedDateTime(String fileLastPrintedDateTime) {
        this.fileLastPrintedDateTime = fileLastPrintedDateTime;
        return this;
    }

    public FileMetadataBuilder setFileCreationDateTime(String fileCreationDateTime) {
        this.fileCreationDateTime = fileCreationDateTime;
        return this;
    }

    public FileMetadataBuilder setFileModificationDateTime(String fileModificationDateTime) {
        this.fileModificationDateTime = fileModificationDateTime;
        return this;
    }

    public FileMetadataBuilder setFileAccessDateTime(String fileAccessDateTime) {
        this.fileAccessDateTime = fileAccessDateTime;
        return this;
    }

    public FileMetadataBuilder setFileSaveDateTime(String fileSaveDateTime) {
        this.fileSaveDateTime = fileSaveDateTime;
        return this;
    }

    public FileMetadataBuilder setMimeType(String mimeType) {
        this.mimeType = mimeType;
        return this;
    }

    public FileMetadataBuilder setTotalEditTime(String totalEditTime) {
        this.totalEditTime = totalEditTime;
        return this;
    }

    public FileMetadataBuilder setTitle(String title) {
        this.title = title;
        return this;
    }

    public FileMetadataBuilder setAuthor(String author) {
        this.author = author;
        return this;
    }

    public FileMetadataBuilder setKeywords(String keywords) {
        this.keywords = keywords;
        return this;
    }

    public FileMetadataBuilder setCompany(String company) {
        this.company = company;
        return this;
    }

    public FileMetadataBuilder setCreator(String creator) {
        this.creator = creator;
        return this;
    }

    public FileMetadataBuilder setModifier(String modifier) {
        this.modifier = modifier;
        return this;
    }

    public FileMetadataBuilder setSubject(String subject) {
        this.subject = subject;
        return this;
    }
    
    public FileMetadataBuilder setPageCount(Long pageCount) {
        this.pageCount = pageCount;
        return this;
    }
    public FileMetadataBuilder setWordCount(Long wordCount) {
        this.wordCount = wordCount;
        return this;
    }
    
    public FileMetadataBuilder setCharacterCount(Long characterCount) {
        this.characterCount = characterCount;
        return this;
    }
    
    public FileMetadataBuilder setLastEditedBy(String lastEditedBy) {
        this.lastEditedBy = lastEditedBy;
        return this;
    }

    public FileMetadataBuilder setLanguage(String language) {
        this.language = language;
        return this;
    }

    public FileMetadataBuilder setDescription(String description) {
        this.description = description;
        return this;
    }

    public FileMetadataBuilder setComments(String comments) {
        this.comments = comments;
        return this;
    }

    public FileMetadataBuilder setFormat(String format) {
        this.format = format;
        return this;
    }

    public FileMetadataBuilder setCreatorTool(String creatorTool) {
        this.creatorTool = creatorTool;
        return this;
    }

    public FileMetadataBuilder setIdentifier(String identifier) {
        this.identifier = identifier;
        return this;
    }

    public FileMetadataBuilder setMetadataDate(String metadataDate) {
        this.metadataDate = metadataDate;
        return this;
    }

    public FileMetadata build() {
        return new FileMetadata(fileName, directory, fileSize,
                fileLastPrintedDateTime,
                fileCreationDateTime,
                fileAccessDateTime,
                fileSaveDateTime, mimeType,
                totalEditTime, title, author, keywords, company, creator,
                modifier, subject, lastEditedBy, characterCount, wordCount,
                pageCount, language, description, comments, creatorTool,
                format, identifier, metadataDate);
    }
}
