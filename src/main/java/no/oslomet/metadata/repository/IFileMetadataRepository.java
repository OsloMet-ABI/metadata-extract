package no.oslomet.metadata.repository;

import no.oslomet.metadata.model.FileMetadata;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IFileMetadataRepository
        extends PagingAndSortingRepository<FileMetadata, Long> {
}
