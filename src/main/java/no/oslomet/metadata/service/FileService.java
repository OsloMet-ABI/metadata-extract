package no.oslomet.metadata.service;

import no.oslomet.metadata.model.FileMetadata;
import no.oslomet.metadata.model.FileMetadataBuilder;
import no.oslomet.metadata.repository.IFileMetadataRepository;

import org.apache.tika.io.TikaInputStream;
import org.apache.tika.metadata.MSOffice;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.metadata.Office;
import org.apache.tika.metadata.TikaCoreProperties;
import org.apache.tika.parser.AutoDetectParser;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.parser.Parser;
import org.apache.tika.sax.BodyContentHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.xml.sax.ContentHandler;

import java.io.File;
import java.nio.file.Path;
import java.util.Arrays;

@Service
public class FileService
        implements IFileService {

    private static final Logger logger = LoggerFactory.
            getLogger(FileService.class);

    private IFileMetadataRepository fileMetadataRepository;

    public FileService(IFileMetadataRepository fileMetadataRepository) {
        this.fileMetadataRepository = fileMetadataRepository;
    }

    public void processDirectory(File dir) {

        Arrays.stream(dir.listFiles((f, n) -> !n.startsWith(".")))
                .forEach(file -> {
                    if (file.isDirectory()) {
                        processDirectory(file);
                    } else {
                        processFile(file.toPath());
                    }
                });
    }

    private void processFile(Path path) {
        try {

            Parser parser = new AutoDetectParser();
            Metadata metadata = new Metadata();
            ContentHandler handler = new BodyContentHandler();
            ParseContext context = new ParseContext();

            TikaInputStream stream = TikaInputStream.get(path);
            parser.parse(stream, handler, metadata, context);

            Long length = path.toFile().length();
            String filename = path.getFileName().toString();
            String mediaType =
                    metadata.get(Metadata.CONTENT_TYPE);

            if (mediaType.equalsIgnoreCase("application/msword")) {

                String title = metadata.get(TikaCoreProperties.TITLE);
                String description = metadata.get(TikaCoreProperties.DESCRIPTION);
                String creator = metadata.get(TikaCoreProperties.CREATOR);
                String comments = metadata.get(TikaCoreProperties.COMMENTS);
                String created = metadata.get(TikaCoreProperties.CREATED);
                String creatorTool =
                        metadata.get(TikaCoreProperties.CREATOR_TOOL);
                String format = metadata.get(TikaCoreProperties.FORMAT);
                String identifier = metadata.get(TikaCoreProperties.IDENTIFIER);
                String keywords = metadata.get(TikaCoreProperties.KEYWORDS);
                String language = metadata.get(TikaCoreProperties.LANGUAGE);
                String metadataDate =
                        metadata.get(TikaCoreProperties.METADATA_DATE);
                String modified = metadata.get(TikaCoreProperties.MODIFIED);
                String modifier = metadata.get(TikaCoreProperties.MODIFIER);
                String printDate = metadata.get(TikaCoreProperties.PRINT_DATE);
                String pageCount = metadata.get(Office.PAGE_COUNT);
                String wordCount = metadata.get(Office.WORD_COUNT);
                String saveDate = metadata.get(Office.SAVE_DATE);
                String editTime = metadata.get(MSOffice.EDIT_TIME);
                String characterCount = metadata.get(Office.CHARACTER_COUNT);
                String initialAuthor = metadata.get(Office.AUTHOR);
                String lastAuthor = metadata.get(Office.LAST_AUTHOR);
                String company = metadata.get(MSOffice.COMPANY);

                FileMetadata file =
                        new FileMetadataBuilder()
                                .setFileCreationDateTime(created)
                                .setFileLastPrintedDateTime(printDate)
                                .setTotalEditTime(editTime)
                                .setAuthor(initialAuthor)
                                .setCompany(company)
                                .setCreator(creator)
                                .setDirectory("")
                                .setFileName(filename)
                                .setFileSaveDateTime(saveDate)
                                .setFileSize(length)
                                .setKeywords(keywords)
                                .setLastEditedBy(lastAuthor)
                                .setWordCount(Long.decode(wordCount))
                                .setPageCount(Long.decode(pageCount))
                                .setCharacterCount(Long.decode(characterCount))
                                .setMimeType(mediaType)
                                .setFileModificationDateTime(modified)
                                .setModifier(modifier)
                                .setLanguage(language)
                                .setTitle(title)
                                .setComments(comments)
                                .setDescription(description)
                                .setCreatorTool(creatorTool)
                                .setFormat(format)
                                .setIdentifier(identifier)
                                .setMetadataDate(metadataDate)
                        .build();
                fileMetadataRepository.save(file);
            }
            stream.close();
        } catch (Exception e) {
            logger.error(e.getLocalizedMessage());
        }
    }
}
