package no.oslomet.metadata.service;

import java.io.File;

public interface IFileService {
    void processDirectory(File dir);
}
